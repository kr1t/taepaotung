<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::resource('admin/league', 'Admin\LeagueController');
Route::resource('admin/league', 'Admin\LeagueController');
Route::resource('admin/club', 'Admin\ClubController');

Route::post('admin/match/{id}/end', 'Admin\MatchController@end');
Route::resource('admin/match', 'Admin\MatchController');

Route::get('profile', 'ProfileController@index');

Auth::routes();


Route::prefix('login')->group(function () {
    Route::get('/{provider}', 'Auth\LoginController@redirectToProvider')->name('login.provider');
    Route::get('/{provider}/callback', 'Auth\LoginController@handleProviderCallback')->name('login.provider.callback');
});


Route::get('/', 'HomeController@index')->name('home');
