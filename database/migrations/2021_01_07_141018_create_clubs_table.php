<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clubs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name')->nullable();
            $table->string('image_url')->nullable();
            $table->integer('match')->nullable();
            $table->integer('win')->nullable();
            $table->integer('draw')->nullable();
            $table->integer('lose')->nullable();
            $table->integer('gf')->nullable();
            $table->integer('ga')->nullable();
            $table->integer('point')->nullable();
            $table->integer('league_id')->unsigned();
            $table->foreign('league_id')->references('id')->on('leagues')->onDelete('cascade')->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clubs');
    }
}
