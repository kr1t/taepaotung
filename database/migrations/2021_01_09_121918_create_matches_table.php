<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('home_id')->unsigned();
            $table->integer('away_id')->unsigned();
            $table->string('rate')->nullable();
            $table->integer('home_goal')->nullable();
            $table->integer('away_goal')->nullable();
            $table->integer('win')->nullable();
            $table->foreign('home_id')->references('id')->on('clubs')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('away_id')->references('id')->on('clubs')->onDelete('cascade')->onUpdate('cascade');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('matches');
    }
}
