<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MatchSelect extends Model
{
    protected $fillable = ['user_id','match_id','select','score'];

    public function user(){
        return $this->belongsTo('App\User');
    }
    public function match()
    {
        return $this->belongsTo('App\Models\Match');
    }
  
}
