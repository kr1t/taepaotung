<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Club;
use App\Models\League;

use Illuminate\Http\Request;

class ClubController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $club = Club::where('name', 'LIKE', "%$keyword%")
                ->orWhere('image_url', 'LIKE', "%$keyword%")
                ->orWhere('match', 'LIKE', "%$keyword%")
                ->orWhere('win', 'LIKE', "%$keyword%")
                ->orWhere('draw', 'LIKE', "%$keyword%")
                ->orWhere('lose', 'LIKE', "%$keyword%")
                ->orWhere('gf', 'LIKE', "%$keyword%")
                ->orWhere('ga', 'LIKE', "%$keyword%")
                ->orWhere('point', 'LIKE', "%$keyword%")
                ->orWhere('league_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $club = Club::latest()->paginate($perPage);
        }

        return view('admin.club.index', compact('club'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $leagues = League::get();
        return view('admin.club.create', compact('leagues'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'image_url' => 'required',
            'match' => 'required',
            'win' => 'required',
            'draw' => 'required',
            'lose' => 'required',
            'gf' => 'required',
            'ga' => 'required',
            'point' => 'required'
        ]);
        $requestData = $request->all();
        if ($request->hasFile('image_url')) {
            $requestData['image_url'] = $request->file('image_url')
                ->store('uploads', 'public');
        }

        Club::create($requestData);

        return redirect('admin/club')->with('flash_message', 'Club added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $club = Club::findOrFail($id);

        return view('admin.club.show', compact('club'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $club = Club::findOrFail($id);
        $leagues = League::get();

        return view('admin.club.edit', compact('club', 'leagues'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'image_url' => 'required',
            'match' => 'required',
            'win' => 'required',
            'draw' => 'required',
            'lose' => 'required',
            'gf' => 'required',
            'ga' => 'required',
            'point' => 'required'
        ]);
        $requestData = $request->all();
        if ($request->hasFile('image_url')) {
            $requestData['image_url'] = $request->file('image_url')
                ->store('uploads', 'public');
        }

        $club = Club::findOrFail($id);
        $club->update($requestData);

        return redirect('admin/club')->with('flash_message', 'Club updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Club::destroy($id);

        return redirect('admin/club')->with('flash_message', 'Club deleted!');
    }
}