<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Match;
use App\Models\Club;
use App\MatchSelect;

use Illuminate\Http\Request;

class MatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function storeApi(Request $request){
        return $request->all();
    }

    public function end(Request $request,$id){
        $requestData = $request->all();

        if(!isset($requestData['win'])){
            return redirect("admin/match/{$id}/edit")->with('flash_message_err', 'Please Select Win Team!');    
        }
        $match = Match::findOrFail($id);

        $match->update($requestData);
        $update = MatchSelect::where('match_id',$id)->where('select',$requestData['win'])->update([
            'score'=>1
        ]);


        return redirect('admin/match')->with('flash_message', 'Match updated!');    }
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $match = Match::where('home_id', 'LIKE', "%$keyword%")
                ->orWhere('away_id', 'LIKE', "%$keyword%")
                ->orWhere('rate', 'LIKE', "%$keyword%")
                ->orWhere('home_goal', 'LIKE', "%$keyword%")
                ->orWhere('away_goal', 'LIKE', "%$keyword%")
                ->with('away','home')
                ->whereNull('win')
                ->latest()->paginate($perPage);
        } else {
            $match = Match::whereNull('win')->
            with('away','home')->latest()->paginate($perPage);
        }

 

        return view('admin.match.index', compact('match'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $club = Club::get();
        return view('admin.match.create',compact('club'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        Match::create($requestData);

        return redirect('admin/match')->with('flash_message', 'Match added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $match = Match::findOrFail($id);

        return view('admin.match.show', compact('match'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $match = Match::findOrFail($id);
        $club = Club::get();

        return view('admin.match.edit', compact('match', 'club'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();
        $requestData['win']= null;
        $match = Match::findOrFail($id);


        $match->update($requestData);

        return redirect('admin/match/'.$id.'/edit')->with('flash_message', 'Match updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Match::destroy($id);

        return redirect('admin/match')->with('flash_message', 'Match deleted!');
    }
}