<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Match;
use App\MatchSelect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
    $matches = Match::whereNull('win')->with('away','home')->latest()->paginate(20);

    $user_id = $request->user()->id;

    if(count($matches)){


    foreach($matches as $m){
        $w = MatchSelect::where('match_id',$m->id)->where('user_id',$user_id)->count();
        $m->canSelect = $w ? false : true;
    }
        }
        return view('home',compact('matches'));
    }
}
