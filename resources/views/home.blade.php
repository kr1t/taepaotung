@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">รายการการแข่งขัน</div>

                <div class="card-body">



                    <div>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">ทีมเหย้า</th>
                                    <th scope="col">ทีมเยือน</th>
                                    <th scope="col">อัตราต่อรอง</th>
                                    <th scope="col">ทายผล</th>
                                </tr>
                            </thead>

                            @foreach($matches as $m)

                            @if($m)
                            <tbody>
                                <tr>
                                    <th scope="row">

                                        <img src="{{ url('/storage/app/public/'.$m->home->image_url) }}" width="50"
                                            height="50" alt="">
                                        {{$m->home->name}}


                                        <input type="radio" value="1" name="win-{{$m->id}}" data-id="{{$m->id}}"
                                            class="match-select">
                                    </th>
                                    <td>

                                        <img src="{{ url('/storage/app/public/'.$m->away->image_url) }}" width="50"
                                            height="50" alt="">
                                        {{$m->away->name}}

                                        <input type="radio" name="win-{{$m->id}}" value="2" data-id="{{$m->id}}"
                                            class="match-select">
                                    </td>
                                    <td>
                                        อัตราต่อรอง {{$m->rate}}
                                    </td>
                                    <td>
                                        <input type="hidden" name="win_select" id="win_select-{{$m->id}}" value="0">

                                        @if($m->canSelect)
                                        <button class="btn btn-primary btn_submit" disabled
                                            id="btn_win_select-{{$m->id}}" data-id="{{$m->id}}">ทายผล</button>
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                            @endif
                            @endforeach
                        </table>
                        <!-- {{$m->home->image_url}}
                        {{$m->away->image_url}} -->




                    </div>

                    {{$matches}}


                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    let user_id = {{ Auth:: id()}}
    const init = {
        matchSelect() {
            $('.match-select').change(function () {
                let id = $(this).data('id')
                let val = $(this).val()

                $('#win_select-' + id).val(val)
                $('#btn_win_select-' + id).removeAttr('disabled')
            })
        },
        submitSelect() {
            $('.btn_submit').click(async function () {
                let id = $(this).data('id')
                let val = $('#win_select-' + id).val()
                console.log(id, val)
                const { data } = await axios.post('/api/save_select_match', { match_id: id, select: val, user_id })
                $(this).hide()

            })
        }

    }
    $(document).ready(function () {

        init.matchSelect()
        init.submitSelect()
    })
</script>
@endsection
