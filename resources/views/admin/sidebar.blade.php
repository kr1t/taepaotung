<div class="col-md-3">
    <div class="card">
        <div class="card-header">
            Menu
        </div>

        <div class="card-body">
            <!-- <ul role="tablist">
                <li role="presentation">

                </li>
                <li role="presentation">
                    <a href="">

                    </a>
                </li>
                <li role="presentation">
                    <a href="">

                    </a>
                </li>

            </ul> -->
            <nav class="nav flex-column">
                <a class="nav-link" href="{{ url('/admin/match') }}">
                    Match
                </a>
                <a class="nav-link" href="{{ url('/admin/league') }}">League</a>
                <a class="nav-link" href="{{ url('/admin/club') }}">Club</a>
            </nav>

        </div>
    </div>
</div>
