<div class="form-group {{ $errors->has('home_id') ? 'has-error' : ''}}">
    <label for="home_id" class="control-label">{{ 'Home' }}</label>

        <select name="home_id" id="home_id" class="form-control">
            @foreach($club as $c)
            <option value="{{$c->id}}" {{ isset($match->home_id) ? $match->home_id == $c->id ? 'selected' :'' : ''}}>{{$c->name}}</option>
            @endforeach
        </select>
    {!! $errors->first('home_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('away_id') ? 'has-error' : ''}}">
    <label for="away_id" class="control-label">{{ 'Away' }}</label>
            <select name="away_id" id="away_id" class="form-control">
                @foreach($club as $c)
                <option value="{{$c->id}}" {{ isset($match->away_id) ? $match->away_id == $c->id ? 'selected' :'' :
                    ''}}>{{$c->name}}</option>
                @endforeach
            </select>
    {!! $errors->first('away_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('rate') ? 'has-error' : ''}}">
    <label for="rate" class="control-label">{{ 'Rate' }}</label>
    <input class="form-control" name="rate" type="text" id="rate" value="{{ isset($match->rate) ? $match->rate : ''}}" >
    {!! $errors->first('rate', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('home_goal') ? 'has-error' : ''}}">
    <label for="home_goal" class="control-label">{{ 'Home Goal' }}</label>
    <input class="form-control" name="home_goal" type="number" id="home_goal" value="{{ isset($match->home_goal) ? $match->home_goal : ''}}" >
    {!! $errors->first('home_goal', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('away_goal') ? 'has-error' : ''}}">
    <label for="away_goal" class="control-label">{{ 'Away Goal' }}</label>
    <input class="form-control" name="away_goal" type="number" id="away_goal" value="{{ isset($match->away_goal) ? $match->away_goal : ''}}" >
    {!! $errors->first('away_goal', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">

</div>


