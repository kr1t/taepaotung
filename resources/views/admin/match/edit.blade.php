@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">

                    <div class="card-header">Edit Match #{{ $match->id }}</div>
                    <div class="card-body">
                        @if( Session::has( 'flash_message' ))
                        <div class="alert alert-success" role="alert">

                            {{ Session::get( 'flash_message' ) }}
                       
                        </div>      
                        @endif

                        @if( Session::has( 'flash_message_err' ))

                        <div class="alert alert-danger" role="alert">

                            {{ Session::get( 'flash_message_err' ) }}
                       
                        </div>
                        
                        
                        @endif
                        
                        
                        
                        <a href="{{ url('/admin/match') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/admin/match/' . $match->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}

                            @include ('admin.match.form', ['formMode' => 'edit'])

                        </form>

                        <hr>
                        <form method="POST" action="{{ url('/admin/match/' . $match->id . '/end') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            {{$match->home->name}}
                            <input type="radio" name="win" value="1">
                            vs
                            {{$match->away->name}}

                            <input type="radio" name="win" value="2">
                            <br>

                        <input class="btn btn-success" type="submit" value="End Match">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
