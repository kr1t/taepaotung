<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'Name' }}</label>
    <input class="form-control" name="name" type="text" id="name" value="{{ isset($club->name) ? $club->name : ''}}" required>
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('image_url') ? 'has-error' : ''}}">
    <label for="image_url" class="control-label">{{ 'Image Url' }}</label>
    <input class="form-control" name="image_url" type="file" id="image_url" value="{{ isset($club->image_url) ? $club->image_url : ''}}" required>
    {!! $errors->first('image_url', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('match') ? 'has-error' : ''}}">
    <label for="match" class="control-label">{{ 'Match' }}</label>
    <input class="form-control" name="match" type="number" id="match" value="{{ isset($club->match) ? $club->match : ''}}" required>
    {!! $errors->first('match', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('win') ? 'has-error' : ''}}">
    <label for="win" class="control-label">{{ 'Win' }}</label>
    <input class="form-control" name="win" type="number" id="win" value="{{ isset($club->win) ? $club->win : ''}}" required>
    {!! $errors->first('win', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('draw') ? 'has-error' : ''}}">
    <label for="draw" class="control-label">{{ 'Draw' }}</label>
    <input class="form-control" name="draw" type="number" id="draw" value="{{ isset($club->draw) ? $club->draw : ''}}" required>
    {!! $errors->first('draw', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('lose') ? 'has-error' : ''}}">
    <label for="lose" class="control-label">{{ 'Lose' }}</label>
    <input class="form-control" name="lose" type="number" id="lose" value="{{ isset($club->lose) ? $club->lose : ''}}" required>
    {!! $errors->first('lose', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('gf') ? 'has-error' : ''}}">
    <label for="gf" class="control-label">{{ 'Gf' }}</label>
    <input class="form-control" name="gf" type="number" id="gf" value="{{ isset($club->gf) ? $club->gf : ''}}" required>
    {!! $errors->first('gf', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ga') ? 'has-error' : ''}}">
    <label for="ga" class="control-label">{{ 'Ga' }}</label>
    <input class="form-control" name="ga" type="number" id="ga" value="{{ isset($club->ga) ? $club->ga : ''}}" required>
    {!! $errors->first('ga', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('point') ? 'has-error' : ''}}">
    <label for="point" class="control-label">{{ 'Point' }}</label>
    <input class="form-control" name="point" type="number" id="point" value="{{ isset($club->point) ? $club->point : ''}}" required>
    {!! $errors->first('point', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('league_id') ? 'has-error' : ''}}">
    <label for="league_id" class="control-label">{{ 'League' }}</label>

    <select name="league_id"  id="league_id" class="form-control">
        @foreach($leagues as $l)
        <option value="{{$l->id}}" {{ isset($club->league_id) ? $club->league_id == $club->id ?'selected' : '' : ''}}>{{$l->name}}</option>
        @endforeach
    </select>
    <!-- <input class="form-control" name="league_id" type="number" id="league_id" value="{{ isset($club->league_id) ? $club->league_id : ''}}" > -->
    {!! $errors->first('league_id', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
