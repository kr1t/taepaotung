<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="control-label">{{ 'Name' }}</label>
    <input class="form-control" name="name" type="text" id="name" value="{{ isset($league->name) ? $league->name : ''}}" >
    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('image_url') ? 'has-error' : ''}}">
    <label for="image_url" class="control-label">{{ 'Image Url' }}</label>
    <input class="form-control" name="image_url" type="file" id="image_url" value="{{ isset($league->image_url) ? $league->image_url : ''}}" >
    {!! $errors->first('image_url', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
