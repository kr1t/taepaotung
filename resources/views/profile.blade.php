@extends('layouts.app')

@section('content')
<div class="container">
    <div class="text-center">
        <img src="{{$user->img_url}}" class="rounded" alt="...">
    </div>
    <div class="text-center">
        <br>
        <h5>{{$user->name}}</h5>
        <h4>scores</h4>
        <h2>{{$user->scores}}</h2>

    </div>

    <hr>




    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">ทีมเหย้า</th>
                <th scope="col">ทีมเยือน</th>
                <th scope="col">คะแนน</th>
            </tr>
        </thead>
        @foreach($user->match_selects as $key => $select)
        <tbody>
            <tr>
                <td>{{$select->match->home->name}} {{$select->match->home_goal}}</td>
                <td>{{$select->match->away->name}} {{$select->match->away_goal}}</td>
                <td>{{$select->score}}</td>
            </tr>
        </tbody>
        @endforeach
    </table>
    <br>

</div>
@endsection

@section('script')
<script>

</script>
@endsection
